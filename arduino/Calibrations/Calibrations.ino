
#include <Arduino.h>
#include <EEPROM.h>

#define print7(a1, a2, a3, a4, a5, a6, a7) \
    Serial.print(a1);                      \
    Serial.print(a2);                      \
    Serial.print(a3);                      \
    Serial.print(a4);                      \
    Serial.print(a5);                      \
    Serial.print(a6);                      \
    Serial.println(a7);                    \
    ;
#define print6(a1, a2, a3, a4, a5, a6) \
    Serial.print(a1);                  \
    Serial.print(a2);                  \
    Serial.print(a3);                  \
    Serial.print(a4);                  \
    Serial.print(a5);                  \
    Serial.println(a6);                \
    ;
#define print5(a1, a2, a3, a4, a5) \
    Serial.print(a1);              \
    Serial.print(a2);              \
    Serial.print(a3);              \
    Serial.print(a4);              \
    Serial.println(a5);            \
    ;
#define print4(a1, a2, a3, a4) \
    Serial.print(a1);          \
    Serial.print(a2);          \
    Serial.print(a3);          \
    Serial.println(a4);        \
    ;
#define print3(a1, a2, a3) \
    Serial.print(a1);      \
    Serial.print(a2);      \
    Serial.println(a3);    \
    ;
#define print2(a1, a2)  \
    Serial.print(a1);   \
    Serial.println(a2); \
    ;
#define print1(a1)      \
    Serial.println(a1); \
    ;

// #pragma region FileLogging

// #define num_length(n) (int)std::ceil(std::log10(n))

// #define file_templ(type) type file_sensor_data::

// class file_sensor_data
// {

// private:
//     int unit_id;
//     int sensor_id;
//     int sensor_min;
//     int sensor_max;

// public:
//     file_sensor_data(int unit, int id, int _min, int _max);

//     file_sensor_data(FILE *file_pointer);

//     char *to_string();

//     void write_to_file(FILE *file_pointer);
// };

// file_templ() file_sensor_data(int unit, int id, int _min, int _max) : unit_id(unit), sensor_id(id), sensor_min(_min), sensor_max(_max) {}

// file_templ() file_sensor_data(FILE *file_pointer)
// {
//     fscanf(file_pointer, "%d %d %d %d\n", &unit_id, &sensor_id, &sensor_min, &sensor_max);
// }

// file_templ(char *) to_string()
// {
//     char *arr = new char[num_length(unit_id) + 1 + num_length(sensor_id) + 1 + num_length(sensor_min) + 1 + num_length(sensor_max) + 1];

//     sprintf(arr, "%d %d %d %d\n", unit_id, sensor_id, sensor_min, sensor_max);
// }

// file_templ(void) write_to_file(FILE *file_pointer)
// {

//     fprintf(file_pointer, "%d %d %d %d\n", unit_id, sensor_id, sensor_min, sensor_max);
// }

// #undef file_templ

// #pragma endregion

// #define default_min 0
// #define default_max 1024

// typedef unsigned int uint;
// #define UINT_MAX UINT32_MAX

// uint sens1min = default_min;
// uint sens1max = default_max;
// uint sens2min = default_min;
// uint sens2max = default_max;

// #define sens1pin A0
// #define sens2pin A1

// #define CALIBRATE_WET_PIN 40
// #define CALIBRATE_DRY_PIN 42

// enum calibrateing_states
// {
//     None = 0,
//     Calibrate_Dry = 1,
//     Calibrate_Wet = 2
// };

// int state = calibrateing_states::None;

#pragma region FileLogging

#define file_templ(type) type file_sensor_data::

#define ROM_SIZEOF_INT 4
#define BYTE_SIZE 8

class file_sensor_data
{

private:
    int unit_id;
    int sensor_id;
    int sensor_min;
    int sensor_max;

    void put(int index, int val);

    int get(int index);

public:
    static const int sizeof_sensor_data = 4 * ROM_SIZEOF_INT;

    file_sensor_data(int unit, int id, int _min, int _max);

    file_sensor_data(int index);

    void write_to_rom(int index);

    void print();
};

file_templ() file_sensor_data(int unit, int id, int _min, int _max) : unit_id(unit), sensor_id(id), sensor_min(_min), sensor_max(_max) {}

file_templ() file_sensor_data(int index)
{
    unit_id = get(index);
    sensor_id = get(index + ROM_SIZEOF_INT);
    sensor_min = get(index + ROM_SIZEOF_INT * 2);
    sensor_max = get(index + ROM_SIZEOF_INT * 3);
}

file_templ(void) write_to_rom(int index)
{
    put(index, unit_id);
    put(index + ROM_SIZEOF_INT, sensor_id);
    put(index + ROM_SIZEOF_INT * 2, sensor_min);
    put(index + ROM_SIZEOF_INT * 3, sensor_max);
}

file_templ(void) put(int index, int val)
{
    for (int i = 0; i < ROM_SIZEOF_INT; i++)
    {
        EEPROM.write(index + i, val & 0xFF);
        val >>= BYTE_SIZE;
    }
}

file_templ(int) get(int index)
{
    int res = 0;

    for (int i = 0; i < ROM_SIZEOF_INT; i++)
    {
        res |= EEPROM.read(index + i) << BYTE_SIZE * i;
    }

    return res;
}

file_templ(void) print()
{
    print2("unit id: ", unit_id);
    print2("sensor id: ", sensor_id);
    print2("sensor min: ", sensor_min);
    print2("sensor max: ", sensor_max);
}

#undef file_templ

#pragma endregion

// void put(int index, unsigned int val)
// {
//     EEPROM.write(index, val & 0xFF);
//     EEPROM.write(index + 1, val >> 8);
// }

// unsigned int read(int index)
// {
//     int res = 0;

//     res = EEPROM.read(index);
//     res |= EEPROM.read(index + 1) << 8;

//     return res;
// }

void setup()
{
    Serial.begin(9600);

    // pinMode(sens1pin, INPUT);
    // pinMode(sens2pin, INPUT);

    // pinMode(CALIBRATE_DRY_PIN, INPUT);
    // pinMode(CALIBRATE_WET_PIN, INPUT);

    // Serial.println(read(0));
    // put(0, 11528);
}

void loop()
{

    // bool cal_dry = digitalRead(CALIBRATE_DRY_PIN);
    // bool cal_wet = digitalRead(CALIBRATE_WET_PIN);

    // if (cal_dry && !cal_wet)
    // {
    //     state = Calibrate_Dry;
    // }
    // else if (cal_wet && !cal_dry)
    // {
    //     state = Calibrate_Wet;
    // }
    // else
    // {
    //     state = None;
    // }

    // if (state == None)
    // {
    //     int sens1read = map(analogRead(sens1pin), sens1min, sens1max, 100, 0);
    //     int sens2read = map(analogRead(sens2pin), sens2min, sens2max, 100, 0);

    //     print4("sens1: max: ", sens1max, ", min: ", sens1min);
    //     print4("sens2: max: ", sens2max, ", min: ", sens2min);
    //     print4("sens1: ", sens1read, ", sens2: ", sens2read);

    //     delay(500);
    // }
    // else if (state == Calibrate_Dry)
    // {
    //     sens1max = analogRead(sens1pin);
    //     sens2max = analogRead(sens2pin);
    // }
    // else if (state == Calibrate_Wet)
    // {
    //     sens1min = analogRead(sens1pin);
    //     sens2min = analogRead(sens2pin);
    // }
}
