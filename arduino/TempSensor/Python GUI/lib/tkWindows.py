from lib.imports import *
from lib.defines import *
from lib.utils import *


class SensorFrame(ttk.Frame, Export, Clearable):
    """ frame for sensor data """

    def __init__(self, container, sensorId: int, app=None):
        super().__init__(container)
        self.app = app
        self.sensorData: list[float] = []  # list of sensor data points
        self.Data = self.sensorData
        self.sensorId = sensorId
        self.options = {'padx': 5, 'pady': 5}  # options

        self.fig = Figure(figsize=(5, 5), dpi=100)  # main figure
        self.plot = self.fig.add_subplot()  # plot
        self.plot.set_xlabel(
            f"Time")  # x & y labels
        self.plot.set_ylabel(f"Temperature (C°)")

        self.plotCanvas = FigureCanvasTkAgg(
            self.fig, master=self)  # plot canvas

        self.padLabel = tk.Label(self, text="", font=FONT, padx=5, pady=5)
        self.padLabel.pack(side='top', fill='both')

        self.timeCounter = 0
        self.x = []
        # self.toolbar = NavigationToolbar2Tk(self.plotCanvas, self)  # toolbar
        self.updatePlot()  # update plot and pack

    def updatePlot(self):
        """ updates the plot and repacks everything """

        self.plot.plot(self.x, self.sensorData, "r-")  # plot with red Xs

        self.plotCanvas.draw()  # draw plot canvas
        # self.toolbar.update()  # update toolbar
        self.plotCanvas.get_tk_widget().pack()  # repack plot canvas

    def addDataPoint(self, data):
        """ add a single data point and redraw the plot """
        self.sensorData.append(data)  # add data point
        self.timeCounter += self.app.sensorReadDT.getValue(
            f"sensorFrame{self.sensorId}")
        self.x.append(self.timeCounter)
        self.updatePlot()  # update plot

    def Clear(self):
        self.x = []
        self.sensorData = []
        self.timeCounter = 0
        self.plot.cla()
        return True


class Sensor3DGraph(ttk.Frame, Export, Clearable):

    timeCounter = 0
    X: np.ndarray = None
    """timestamps"""
    Y: np.ndarray = None
    """distances"""
    Z: np.ndarray = None
    """ temperatures per sensor """

    def __init__(self, container, sensorIDs: list[int], sensorPositions: list[int], app=None):
        super().__init__(container)
        self.app = container if app is None else app  # check for app override

        self.sensorIDs = sensorIDs
        """the ID Map of the sensor IDs, physical number->sensor ID"""
        self.sensorDistances = sensorPositions
        """list of sensor positions (distance from heat source)"""
        self.sensorCount = len(self.sensorIDs)
        """count of connected sensors"""

        self.topF = tk.Frame(self)
        """top frame, for buttons and such"""
        self.topF.pack(side='top', fill='both', expand=True)

        self.updateGraphVar = tk.IntVar(self, 1)  # make updateGraph cehckbox
        self.toggle = tk.Checkbutton(
            self.topF, onvalue=1, offvalue=0, variable=self.updateGraphVar, text="Update Graph", font=FONT)
        self.toggle.pack(side='left', fill='both', padx=5, pady=5)

        self.canvas3D()  # draws the canvas

        self.Data = lambda: (self.X, self.Y, self.Z)

        # add a listener for sensor data
        self.app.lockedSensorData.addUpdateStateListener(
            "3dGraph", self.addData
        )

    def canvas3D(self, master=None, side='bottom'):
        """setup the 3D canvas"""
        self.fig = Figure(figsize=(5, 5), dpi=100)

        # make the canvas around the figure
        self.canvas = FigureCanvasTkAgg(
            self.fig, master=self if master is None else master)
        self.canvas.draw()

        self.ax = self.fig.add_subplot(111, projection="3d")  # add axes

        self.ax.set_xlabel("Time")  # set axes labels
        self.ax.set_ylabel("Distance")
        self.ax.set_zlabel("Temperature")

        self.X = np.array([])  # set inital data arrays
        self.Y = np.array([])
        self.Xm, self.Ym = np.meshgrid(self.X, self.Y)  # set meshgrid vars
        self.Z = np.array([[]])

        self.minz = None  # init min&max z
        self.maxz = None

        # plot surface
        self.surf = self.ax.plot_surface(self.Xm, self.Ym, self.Z, cmap=cm.coolwarm,
                                         linewidth=0, antialiased=False)
        self.ax.set_zlim(self.minz, self.maxz)  # set z axes limits

        self.colorbar = self.fig.colorbar(
            self.surf, shrink=0.5, aspect=10)  # make colorbar
        self.canvas.get_tk_widget().pack(side=side, fill='both', expand=True)  # pack canvas

    def update(self, transpose=True):
        """updates the 3D graph"""
        if self.updateGraphVar.get():  # if updateGraph is set
            self.canvas.draw()  # redraws the canvas
            self.surf = self.ax.plot_surface(self.Xm, self.Ym, np.transpose(self.Z) if transpose else self.Z, cmap=cm.coolwarm,
                                             linewidth=0, antialiased=False)
            self.ax.set_zlim(0 if self.minz is None else self.minz,
                             1 if self.maxz is None else self.maxz)
            self.colorbar.remove()
            self.colorbar = self.fig.colorbar(
                self.surf, shrink=0.5, aspect=10)

        self.canvas.get_tk_widget().pack(side='top', fill='both', expand=True)

    def addData(self, owner: str, data: list[float]):
        """ adds sensor data and replots the graph """
        # print(self.X, self.Y, self.Z)
        if not np.any(self.Z):  # if Z iz empty
            self.Z = np.array([data])  # init z
        else:  # if there is any data
            # append newdata
            self.Z = np.append(self.Z, [[data[i-1]
                               for i in self.sensorIDs]], axis=0)

        minData = np.min(data)  # calculate max and min values for z limit
        maxData = np.max(data)
        self.minz = minData if self.minz is None else min(minData, self.minz)
        self.maxz = maxData if self.maxz is None else max(maxData, self.maxz)

        if len(self.X) == 0:  # if x is empty
            self.X = np.array([0])  # init
        else:  # else
            # add new data point
            self.timeCounter += self.app.sensorReadDT.getValue(
                "3dgraph")  # ms to seconds
            self.X = np.append(self.X, [self.timeCounter])
        if len(self.Y) == 0:  # if y is empty
            # init with distance values
            self.Y = np.array(self.sensorDistances)

        self.Xm, self.Ym = np.meshgrid(self.X, self.Y)  # recalculate meshgrid

        self.update()  # update the graph

    def Clear(self):
        self.X = np.array([])  # reset inital data arrays
        self.Y = np.array([])
        self.Xm, self.Ym = np.meshgrid(self.X, self.Y)  # reset meshgrid vars
        self.Z = np.array([[]])

        self.timeCounter = 0

        self.minz = None  # reinit min&max z
        self.maxz = None
        self.ax.set_zlim(0, 1)
        self.ax.cla()
        # self.update()
        return True


class SensorDistances(ttk.Frame, Saveable):

    def __init__(self, container, sensorCount):
        super().__init__(container)
        self.app = container
        self.sensorCount = sensorCount

        self.savePath = SAVE_FILE
        self.tag = "sensorDistances"
        self.setDebug(True)
        self.sensorPosVars = []

        self.app.title("Sensor Positions")

        labelF = tk.Frame(self)  # the label frame
        labelF.grid(row=0, column=0)  # pack
        # make the label
        self.label = tk.Label(
            labelF, text="For each sensor set how far it is from the heat source.", justify='left', font=FONT)
        self.label.pack(fill='both', side='left', padx=5, pady=5)  # pack

        # the sensors scrollable frame, 610 being the correct width for 3 sensor windows one by one
        self.sensF = ScrollableFrame(self, width=660)

        for i in range(1, self.sensorCount+1):  # iterate over sensor count
            # place 3 items per row
            x = (m if (m := (i % 3)) != 0 else 3)-1
            y = (i-1) // 3  # the remaining height

            # label frame for sensor
            lf = tk.LabelFrame(self.sensF.scrollable_frame,
                               text=f"Physical Sensor #{i}", font=FONT)
            lf.grid(column=x, row=y)  # place in grid

            # position label
            PositionL = tk.Label(
                lf, text="Distance:", font=FONT)
            PositionL.pack(fill='both', side='left', padx=15, pady=10)

            # position variable
            PositionVar = tk.DoubleVar(lf, 0.0)
            # position spinbox
            PositionSB = tk.Spinbox(
                lf, from_=0, to=pow(2, 64)-1, textvariable=PositionVar, font=FONT, width=10, wrap=True, increment=0.05)
            PositionSB.pack(fill='both', side='right',
                            padx=10, pady=10)
            self.sensorPosVars.append(PositionVar)  # add var to list

        self.sensF.grid(row=1, column=0, padx=5)  # place sensors frame in grid

        # buttons: ###############

        buttonsF = tk.Frame(self)  # buttons main frame
        buttonsF.grid(row=2, column=0, sticky='NSEW', padx=5, pady=5)

        # right buttons frame
        buttonsRF = tk.Frame(buttonsF)
        buttonsRF.pack(side='right', fill='both')

        # reset IDs button
        resetBtn = tk.Button(
            buttonsRF, text="Reset", command=self.reset, font=FONT)
        resetBtn.pack(side='left', fill='both', padx=5)

        # confirm IDs button
        confirmBtn = tk.Button(
            buttonsRF, text="Confirm", command=self.confirm, font=FONT)
        confirmBtn.pack(side='right', fill='both', padx=5)

        # left buttons frame
        buttonsLF = tk.Frame(buttonsF)
        buttonsLF.pack(side='left', fill='both')

        # save IDs to file button
        saveBtn = tk.Button(buttonsLF, text="Save Positions",
                            command=self.save, font=FONT)
        saveBtn.pack(side='left', fill='both', padx=5)

        # load IDs from file button
        loadBtn = tk.Button(
            buttonsLF, text="Load Saved Positions", command=self.load, font=FONT)
        loadBtn.pack(side='right', fill='both', padx=5)

    def reset(self):
        for var in self.sensorPosVars:
            var.set(0)

    def getVals(self):
        r = []
        try:
            r = [v.get() for v in self.sensorPosVars]
        except tk.TclError:
            messagebox.showerror(
                "Parsing Error", "One or more of the position inputs is invalid! (probably empty)\nMake sure all position values are correct and try again.")
            return None
        return r

    def confirm(self):
        if (ret := self.getVals()) is None:
            return
        self.app.message(APP_CODE_SENSOR_DISTANCE_SET, ret)
        # return ret

    def getSaveData(self) -> tuple[Any, bool]:
        vs = self.getVals()
        if vs is None:
            return (None, False)
        return ("\n".join([str(v) for v in vs]), True)

    def parseData(self, data: str) -> bool:
        data = data.split("\n")
        try:
            data = [float(d) for d in data]
        except ValueError:
            return False
        for i, var in enumerate(self.sensorPosVars):
            if i >= len(data):
                return False
            var.set(data[i])
        return True


class ThermalConductivity(ttk.Frame, Saveable, Export, Clearable):
    def __init__(self, container, sensorIDs, sensorDistances, app):
        super().__init__(container)
        self.sensorIDs = sensorIDs  # save sensor IDs
        self.sensorCount = len(sensorIDs)  # get sensor count
        self.app = container.app if hasattr(
            container, 'app') else app  # get app/app override param
        self.sensorDistances = sensorDistances  # get sensor distances

        self.savePath = SAVE_FILE  # set save path
        self.tag = "thermalConductivity"

        self.KnownK = 0
        """known material Thermal conductivity"""
        self.measuredK = 0
        """measured Thermal conductivity of sample material"""
        self.measuredQ = 0
        """measured heat flux"""
        self.KM_sensors = []
        """IDs of known material sensors"""
        self.TM_sensors = []
        """IDs of sample material sensors"""
        self.process = False
        """if set, processing senesor data"""

        label = tk.Label(self, text="Place the sensors on the Sample and the Known materials in numerical\norder, and mark which sensors are used in each material. (e.g. sensors\n1,2,4 for known material and sensors 3,5,6 for sample material)\nThen set the known material's Thermal Conductivity, and press Confirm.",
                         font=FONT, justify='left').pack(side='top', fill='both', padx=5, pady=15)

        labelFs = tk.Frame(self)
        labelFs.pack(side='top', fill='both', expand=True)

        # known material frame:
        self.makeKnownMaterialWidget(labelFs)
        # test material frame:
        self.makeSampleMaterialWidget(labelFs)

        # buttons frame
        buttonsF = tk.Frame(self)
        buttonsF.pack(side='bottom', fill='both')

        self.makeButtons(buttonsF)

        self.app.lockedSensorData.addUpdateStateListener(
            "thermalConductivity", self.sensorDataChanged)

    def makeSensorList(self, parent, build_kwargs, side):
        """ makes the sensor checkbox scrollable list widget """
        Sensors = ScrollableFrame(
            parent, height=200, width=150)  # scrollable frame

        # return list
        checkButtons: list[tuple[tk.Checkbutton, tk.IntVar]] = []

        for i in range(self.sensorCount):  # iterate over sensor count
            var = tk.IntVar(Sensors, 0)  # make a new var
            # make a checkbutton, with correct number, and set the command to checkButtonToggled
            cb = tk.Checkbutton(Sensors.scrollable_frame,
                                text=f"Use {i+1}{'th' if (v:=(i+1)%10) > 3 else 'rd' if v == 3 else 'nd' if v == 2 else 'st' if v == 1 else 'th'} Sensor", variable=var, font=FONT, command=lambda index=i, v=var: self.checkButtonToggled(index, side, v))
            cb.pack(padx=5, pady=5, fill='both')  # pack
            checkButtons.append((cb, var))  # add values to list
        Sensors.grid(**build_kwargs)  # grid the scrollable frame
        return checkButtons  # retrurn check buttons and variables

    def makeKnownMaterialWidget(self, parent):
        knownMatLF = tk.LabelFrame(
            parent, text="Known Material", font=FONT_LARGE)
        knownMatLF.pack(side='left', fill='both', expand=True, padx=5, pady=5)

        kvalLabel = tk.Label(knownMatLF, text="Known K Value:", font=FONT, justify='left').grid(
            row=0, column=0, padx=5, pady=5)

        self.kValInputVar = tk.DoubleVar(knownMatLF, 0.0)
        self.kvalInput = FastSpinbox(knownMatLF, from_=0, to=1E6, increment=0.001,
                                     width=10, textvariable=self.kValInputVar, font=FONT, state='readonly')
        self.kvalInput.grid(row=0, column=1, padx=5, pady=5)
        mesQLabel = tk.Label(knownMatLF, text="Measured Q:", font=FONT, justify='left').grid(
            row=1, column=0, padx=5, pady=5)

        self.knownMatMesQVar = tk.DoubleVar(knownMatLF, 0.0)
        mesQDispLabel = tk.Label(knownMatLF, textvariable=self.knownMatMesQVar, font=FONT, justify='right').grid(
            row=1, column=1, padx=5, pady=5)

        sensorsLabel = tk.Label(knownMatLF, text="Sensors",
                                font=FONT_LARGE, justify='left').grid(row=2, columnspan=2)
        sensorsLabel2 = tk.Label(knownMatLF, text="Nth Placement:",
                                 font=FONT, justify='left',).grid(row=3, column=0)
        sensorsLabel3 = tk.Label(knownMatLF, text="Physical Sensor\nNumber:",
                                 font=FONT, justify='left',).grid(row=3, column=1)

        KMSensors = ScrollableFrame(knownMatLF, height=200, width=150)

        # self.KM_CheckVars: list[tk.IntVar] = []
        self.KM_CheckButtons: list[tuple[tk.Checkbutton, tk.IntVar]] = self.makeSensorList(knownMatLF, {
            "row": 4,
            "column": 0,
            "rowspan": 5,
            "columnspan": 2,
            "padx": 5,
            "pady": 5
        }, 1)

    def makeSampleMaterialWidget(self, parent):
        testMatLF = tk.LabelFrame(
            parent, text="Test Material", font=FONT_LARGE)
        testMatLF.pack(side='right', fill='both', expand=True, padx=5, pady=5)

        kvalLabel = tk.Label(testMatLF, text="Calculated K Value:", font=FONT, justify='left').grid(
            row=0, column=0, padx=5, pady=5)

        self.kValOutputVar = tk.DoubleVar(testMatLF, 0.0)
        kvalInput = tk.Label(testMatLF, textvariable=self.kValOutputVar, font=FONT, justify='right').grid(
            row=0, column=1, padx=5, pady=5)

        mesQLabel = tk.Label(testMatLF, text="Measured Q:", font=FONT, justify='left').grid(
            row=1, column=0, padx=5, pady=5)

        self.testMatMesQVar = tk.DoubleVar(testMatLF, 0.0)
        mesQDispLabel = tk.Label(testMatLF, textvariable=self.testMatMesQVar, font=FONT, justify='right').grid(
            row=1, column=1, padx=5, pady=5)

        sensorsLabel = tk.Label(testMatLF, text="Sensors",
                                font=FONT_LARGE, justify='left').grid(row=2, columnspan=2)
        sensorsLabel2 = tk.Label(testMatLF, text="Nth Placement:",
                                 font=FONT, justify='left',).grid(row=3, column=0)
        sensorsLabel3 = tk.Label(testMatLF, text="Physical Sensor\nNumber:",
                                 font=FONT, justify='left',).grid(row=3, column=1)

        self.TM_CheckButtons: list[tuple[tk.Checkbutton, tk.IntVar]] = self.makeSensorList(testMatLF, {
            "row": 4,
            "column": 0,
            "rowspan": 5,
            "columnspan": 2,
            "padx": 5,
            "pady": 5
        }, 0)

    def makeButtons(self, parent):
        buttonsF = parent

        saveBtn = tk.Button(
            buttonsF, text="Save Values", command=self.save, font=FONT).pack(side='left', fill='both', padx=5, pady=5)

        loadBtn = tk.Button(
            buttonsF, text="Load Values", command=self.load, font=FONT).pack(side='left', fill='both', padx=5, pady=5)

        # sideBuffer = tk.Label(buttonsF).pack(padx=50)

        self.confirmValsBtn = tk.Button(
            buttonsF, text="Confirm Values", command=self.confirm, font=FONT)
        self.confirmValsBtn.pack(
            side='right', fill='both', padx=5, pady=5)

        self.clearBtn = tk.Button(
            buttonsF, text="Clear", command=self.clear, font=FONT)
        self.clearBtn.pack(side='right', fill='both', padx=5, pady=5)

    def checkButtonToggled(self, index: int, side: int, var: tk.IntVar):
        """called on sensor checkbutton toggle"""
        # print(index, side)
        cb: tk.Checkbutton = [self.TM_CheckButtons,
                              self.KM_CheckButtons][::-1][side][index][0]  # get respective button on the other material
        cb.deselect()  # deselect respective button
        # if toggled button is now set, set respective button to disabled, otherwise set it as normal
        cb.configure(state="disabled" if var.get() else "normal")

    def clear(self):
        """called through clear button"""
        if self.process:  # if processing
            return  # exit
        self.kValInputVar.set(0)  # reset K
        for (km, _), (tm, _) in zip(self.KM_CheckButtons, self.TM_CheckButtons):  # iterate over checkbuttons
            km.deselect()  # deselect
            km.configure(state='normal')  # set to normal state(not disabled)
            tm.deselect()
            tm.configure(state='normal')

    def confirm(self):
        """called with the confirm values button"""
        self.kvalInput.configure(state='disabled')  # disable k value input

        for (km, _), (tm, _) in zip(self.KM_CheckButtons, self.TM_CheckButtons):  # iterate over checkbuttons
            km.configure(state='disabled')  # disable them
            tm.configure(state='disabled')

        # disable clear & confirm buttons
        self.clearBtn.configure(state='disabled')
        self.confirmValsBtn.configure(state='disabled')

        self.KnownK = self.kValInputVar.get()  # fetch values
        self.KM_sensors = [
            i for i, (cb, var) in enumerate(self.KM_CheckButtons) if var.get()]
        self.TM_sensors = [
            i for i, (cb, var) in enumerate(self.TM_CheckButtons) if var.get()]
        self.process = True

    def getSaveData(self) -> tuple[Any, bool]:
        k = self.kValInputVar.get()  # fetch data
        KM_sensors = [
            var.get() for i, (cb, var) in enumerate(self.KM_CheckButtons)]
        TM_sensors = [
            var.get() for i, (cb, var) in enumerate(self.TM_CheckButtons)]

        r = str(k)+"\n"  # save in string
        r += f"[{', '.join([str(i) for i in KM_sensors])}]\n"
        r += f"[{', '.join([str(i) for i in TM_sensors])}]"
        return (r, True)

    def parseData(self, data: str) -> bool:
        if self.process:  # if in process
            # error
            self.setError("Can't load data while measurment is taking place.")
            return False
        try:  # catch errors
            # split over \n into k, knownMatSensors, testMatSensors
            k, km_sensors, tm_sensors = data.split("\n")
            k = float(k)  # parse k
            km_sensors = eval(km_sensors)  # eval sensor lists
            tm_sensors = eval(tm_sensors)

            self.kValInputVar.set(k)  # set k
            for i, (km, tm) in enumerate(zip(km_sensors, tm_sensors)):  # set set values to checkbuttons
                self.KM_CheckButtons[i][1].set(km)
                self.TM_CheckButtons[i][1].set(tm)
                if km:  # disable button if other side is toggled
                    self.TM_CheckButtons[i][0].configure(state="disabled")
                if tm:
                    self.KM_CheckButtons[i][0].configure(state="disabled")
        except:  # got any exception
            self.setError(
                "Got exception while trying to parse, save file is probably invalid or corrupt.")
            return False  # failed to parse
        return True  # parsed successfully

    def Data(self):
        if not self.process:  # if not in process, no data to export
            return None
        return [self.KnownK, self.measuredK, self.measuredQ]  # export k and q

    def calculateK(self, dt1, dt2, dx1, dx2):
        """
        subscript 1₁ is SMAPLE
        subscript 2₂ is STANDARD(Known Material)
        I ASSUME A₁=A₂, THEREFOR A₁/A₂ = 1
        k₁=k₂(A₂∆T₂L₁)/(A₁∆T₁L₂)
        """
        # print("calculate K:", self.KnownK, dt1, dt2, dx1, dx2)
        if dt1 == 0 or dx2 == 0:
            # messagebox.showerror(
            #     "Zero Values", f"Calculate K got 0 values, dt1:{dt1},dx2:{dx2}")
            print("calculate k got dt1 or dx2 zeros")
            return 0
        return self.KnownK*(dt2*dx1)/(dt1*dx2)

    def calculateQ(self, k, dt, dx):
        """
        subscript 1₁ is SMAPLE
        subscript 2₂ is STANDARD(Known Material)
        q = -k∆T/∆x
        """
        if dx == 0:
            print("calculate q got dx 0")
            return 0
        return -k*dt/dx

    def getDs(self, iter):
        """ calculates a Delta list of values in iter, iter must be of length of 2+ """
        ds = []
        for i in range(0, len(iter)-1):
            ds.append(iter[i]-iter[i+1])
        return ds

    def sensorDataChanged(self, owner, data):
        """
        subscript 1₁ is SMAPLE
        subscript 2₂ is STANDARD(Known Material)
        I ASSUME SENSORS ARE PLACED IN A NUMERICAL ORDER FROM HOTTER AREAS TO COLDER AREAS

        """
        if not self.process:
            return
        temps = data

        # get ∆Ts and ∆Xs of known mat:
        T2s = [temps[self.sensorIDs[idmap]-1] for idmap in self.KM_sensors]
        DT2s = self.getDs(T2s)
        X2s = [self.sensorDistances[id] for id in self.KM_sensors]
        DX2s = self.getDs(X2s)

        # get ∆Ts and ∆Xs of sample mat:
        T1s = [temps[self.sensorIDs[idmap]-1] for idmap in self.TM_sensors]
        DT1s = self.getDs(T1s)
        X1s = [self.sensorDistances[id] for id in self.TM_sensors]
        DX1s = self.getDs(X1s)

        # calculate K of sample mat:
        Ks = []
        for tup in zip(DT1s, DT2s, DX1s, DX2s):
            Ks.append(self.calculateK(*tup))
        AvgK = np.average(Ks)  # get average result
        # print(Ks, AvgK)

        # calculate Q of both sample and known mat
        Qs = []
        for tup in zip([AvgK, ]*len(DT1s), DT1s, DX1s):
            Qs.append(self.calculateQ(*tup))
        AvgQ = round(np.average(Qs), KQ_ACCURACY)  # get average Q
        AvgK = round(AvgK, KQ_ACCURACY)
        self.kValOutputVar.set(AvgK)  # set output vars
        self.testMatMesQVar.set(AvgQ)
        self.knownMatMesQVar.set(AvgQ)
        self.measuredK = AvgK
        self.measuredQ = AvgQ

    def Clear(self):
        self.process = False
        self.clear()
        self.kValOutputVar.set(0)
        self.testMatMesQVar.set(0)
        self.knownMatMesQVar.set(0)
        self.kvalInput.configure(state='normal')

        self.confirmValsBtn.configure(state='normal')
        self.clearBtn.configure(state='normal')
        return True


class SensorGraphs(ttk.Frame):
    """ stacked frame window of 3D sensor graph, sensor readout data, and each sensor data """

    def __init__(self, container, idMap: list[int], distances: list[float]):
        super().__init__(container)
        self.app = container
        self.idMap = idMap  # ids of sensors
        self.sensorCount = len(self.idMap)
        self.sensorDistances = distances  # sensor distances

        self.ExportPath = "ExportedData.xlsx"

        self.SF = StackedFrame(self)  # stacked frame

        self.sensorFrames: list[SensorFrame] = []
        """list of all sensor frames"""

        self.thermalData: ThermalConductivity = self.SF.addWindow("Thermal Conductivity", ThermalConductivity, {
                                                                  "sensorIDs": self.idMap, "sensorDistances": distances, "app": self.app})[1]

        self.graph3d: Sensor3DGraph = self.SF.addWindow("3D Graph", Sensor3DGraph, {
            "sensorIDs": self.idMap, "sensorPositions": distances, "app": self.app})[1]
        """the 3D graph Sensor3DGraph class"""

        for i, val in enumerate(self.idMap):  # iterate over sensor ids
            self.sensorFrames.append(self.SF.addWindow(
                f"Sensor #{i+1}", SensorFrame, {"sensorId": val, "app": self.app})[1])  # add new SensorFrame to StackedFrame

        self.app.lockedSensorData.addUpdateStateListener(  # add new update state listener
            "SensorGraphs", self.sensorDataChange)

        # setup menu and menu options
        self.menubar = tk.Menu(self.app, font=FONT)
        self.dataMenu = tk.Menu(self.menubar, font=FONT, tearoff=0)
        self.dataMenu.add_command(
            label="Export To File", command=self.exportAllDataToFile, font=FONT)
        self.dataMenu.add_command(
            label="Clear Data", command=self.clearData, font=FONT)
        self.menubar.add_cascade(
            label="Data", menu=self.dataMenu, font=FONT_LARGE)
        self.app.config(menu=self.menubar)

    def sensorDataChange(self, _, data):
        """update state listener for sensor data"""
        # print("got data")
        for i, sensor in enumerate(self.sensorFrames):
            # print(i, sensor.sensorId, data[sensor.sensorId-1])
            sensor.addDataPoint(data[sensor.sensorId-1])

    def exportAllDataToFile(self) -> bool:
        """exports all sensor data to file"""
        data = self.graph3d.exportData()  # get 3d sensor data
        times: np.ndarray = data[0]
        distances: np.ndarray = data[1]
        temperatures: np.ndarray = data[2]
        thermal = self.thermalData.exportData()
        if thermal is None:
            messagebox.showerror(
                "Error Exporting Data", "Cannot export while Thermal Conductivity Window is not processing data.")
            return False
        knownk, k, q = thermal
        if os.path.isfile(self.ExportPath) and not messagebox.askyesnocancel("Exported Data Already Exists", "Exported Data File Already Exists, Do you want to overload it?"):
            return False

        # print("export all data to file")
        # print("times:", times)
        # print("distances:", distances)
        # print(temperatures)
        sensors = [None, ] * self.sensorCount
        for temps in temperatures:
            # print(temps)
            for i in range(self.sensorCount):
                # print(sensors[i], temps[i])
                if sensors[i] is None:
                    sensors[i] = []
                sensors[i].append(temps[i])
        SL = len(sensors[0])+1

        none_pad = [None, ] * (SL-1)

        d = {
            "Known K Value": [knownk, ] + none_pad,
            "Calculated Q": [q, ] + none_pad,
            "Caluclated K (on test material)": [k, ] + none_pad,
            "Time Stamps": [None, ] + times.tolist(),
        }
        for i, (dis, sensor) in enumerate(zip(distances, sensors)):
            d[f"Sensor #{i+1}"] = [f"Distance: {dis}", ]+sensor

        pd.DataFrame(d).to_excel(self.ExportPath)

        # pd.DataFrame({
        #     "Timestamps": [times, ]
        # }).to_excel(self.ExportPath, startrow=4, index=False)
        # pd.DataFrame({f"Sensor #{i+1}": [str(temps), ]
        #              for i, temps in enumerate(sensors)}).to_excel(self.ExportPath, startrow=7, index=False)

        # print("temperatures:", sensors)
        # print("k,q:", k, q)
        # self.__write_data_to_file(k, q, distances, times, sensors)
        messagebox.showinfo("Export Successful",
                            "Exported data to file " + self.ExportPath)

    def __write_data_to_file(self, k, q, distances, times, sensors):
        newline = '\n'
        res = f"""Calculated K of Sample Material: {k}
Calculated Q of Both Sample and Standard Material: {q}

Sensor Data:
Sensor Distances: {distances}
Measurement Timestamps: [{', '.join([str(s) for s in times])}]
Temperature Data (T/timestamp):
{newline.join([f"Sensor #{i+1}: {str(temps)}" for i,temps in enumerate(sensors)])}"""
        with open(self.ExportPath, "w+") as file:
            file.write(res)

    def clearData(self):
        if not messagebox.askyesnocancel("Clear All Data", "Are you sure you want to clear all data? This action cannot be reversed."):
            return
        self.thermalData.Clear()
        self.graph3d.Clear()
        for sensor in self.sensorFrames:
            sensor.Clear()
        messagebox.showinfo("Cleared all data", "Cleared all data.")


class FindComPort(ttk.Frame):
    """ window for selecting the proper com port of the arduino """

    def __init__(self, container):
        super().__init__(container)
        self.app = container

        self.ports = serial_ports()  # get ports

        self.app.title("Find Serial Port")  # set title
        self.label = tk.Label(
            self, text="Select the correct serial port of the arduino board:", font=FONT)  # init and pack label
        self.label.pack(fill='both', side='top', padx=5, pady=5)

        # list section

        self.listFrame = tk.Frame(self)  # list frame
        self.listFrame.pack(fill='both', padx=5, pady=5)

        self.listScroll = tk.Scrollbar(self.listFrame)  # scroll wheel
        self.listScroll.pack(fill='y', side="right")

        self.list = tk.Listbox(
            self.listFrame, yscrollcommand=self.listScroll.set, font=FONT)  # init list

        for i, (p, _) in enumerate(self.ports):  # add ports
            self.list.insert(i, p)

        self.list.pack(fill='both', padx=5, pady=5)  # pack list

        # link scroll wheel to list
        self.listScroll.config(command=self.list.yview)

        # buttons frame section

        self.btnFrame = tk.Frame(self)  # button frame
        self.btnFrame.pack(fill='both', side='bottom', padx=5, pady=5)  # pack

        self.refrehBtn = tk.Button(
            self.btnFrame, text="Refresh", command=lambda: self.refresh(), font=FONT)  # refresh button, and pack
        self.refrehBtn.pack(fill='both', side='left',
                            padx=5, pady=5)

        self.confirmBtn = tk.Button(
            self.btnFrame, text="Confirm", command=lambda: self.confirm(), font=FONT)  # confirm button
        self.confirmBtn.pack(fill='both', side='right',
                             padx=5, pady=5)  # pack

    def refresh(self):
        """ refreshes the lists' values """
        self.list.delete(0, len(self.ports))  # clear the list
        self.ports = serial_ports()  # get ports
        for i, (p, n) in enumerate(self.ports):  # add ports to list
            self.list.insert(i, p)

    def confirm(self):
        """ confirms the selection and moves on """
        s = self.list.curselection()  # get selected indices
        if len(s) == 0:  # if no selection
            return  # ignore

        port = self.ports[s[0]][0]  # get port at index
        self.app.serialPort = port  # set app port
        # print(port)

        self.app.message(APP_CODE_INIT_HANDSHAKE)  # initialize board handshake


class SetSensorIDs(ttk.Frame, Saveable):
    def __init__(self, container, sensorCount):
        super().__init__(container)
        self.app = container
        self.sensorCount = sensorCount

        self.savePath = SAVE_FILE
        self.tag = "sensorIDs"

        # the temperature labels for the sensors
        self.sensorTemps: list[tk.StringVar] = []
        self.sensorIDs: list[tk.IntVar] = []  # the sensor id scrollboxes

        labelF = tk.Frame(self)  # the label frame
        labelF.grid(row=0, column=0)  # pack
        # make the label
        self.label = tk.Label(
            labelF, text="For each sensor, warm it up/cool it down, find which temperature\nreading changes, and change the 'Physical Number' to the number\nwritten on the physical sensor.", justify='left', font=FONT)
        self.label.pack(fill='both', side='left', padx=5, pady=5)  # pack

        # the sensors scrollable frame, 610 being the correct width for 3 sensor windows one by one
        self.sensF = ScrollableFrame(self, width=660)

        for i in range(1, self.sensorCount+1):  # iterate over sensor count
            # place 3 items per row
            x = (m if (m := (i % 3)) != 0 else 3)-1
            y = (i-1) // 3  # the remaining height

            # label frame for sensor
            lf = tk.LabelFrame(self.sensF.scrollable_frame,
                               text=f"Physical Sensor #{i}", font=FONT)
            lf.grid(column=x, row=y)  # place in grid

            tempf = tk.Frame(lf)  # temperature frame
            tempf.pack(fill='both', side='bottom', padx=5, pady=5)

            templ = tk.Label(tempf, text="Temperature:",
                             font=FONT)  # temperature label
            templ.pack(fill="both", side='left', padx=5, pady=5)

            # add new temperature value to list
            self.sensorTemps.append(tk.StringVar(tempf, name=f"Sensor #{i}"))
            # temperature value label
            temp = tk.Label(
                tempf, textvariable=self.sensorTemps[-1], font=FONT)
            temp.pack(fill='both', side='right', padx=5, pady=5)

            actualIDF = tk.Frame(lf)  # physical number frame
            actualIDF.pack(fill='both', side='top', padx=10, pady=5)

            # physical number label
            actualIDL = tk.Label(
                actualIDF, text="Actual Sensor ID:", font=FONT)
            actualIDL.pack(fill='both', side='left', padx=5, pady=5)

            actualIDVar = tk.IntVar(actualIDF, i)  # physical number variable
            # physical number spinbox
            actualIDSBox = tk.Spinbox(
                actualIDF, from_=1, to=self.sensorCount, textvariable=actualIDVar, command=lambda i=i: self.idChange(i), font=FONT, width=5, wrap=True, state='readonly')
            actualIDSBox.pack(fill='both', side='right',
                              padx=5, pady=5)
            self.sensorIDs.append(actualIDVar)  # add var to list

        self.sensF.grid(row=1, column=0, padx=5)  # place sensors frame in grid

        # buttons: ###############

        buttonsF = tk.Frame(self)  # buttons main frame
        buttonsF.grid(row=2, column=0, sticky='NSEW', padx=5, pady=5)

        # right buttons frame
        buttonsRF = tk.Frame(buttonsF)
        buttonsRF.pack(side='right', fill='both')

        # reset IDs button
        resetBtn = tk.Button(
            buttonsRF, text="Reset", command=self.reset, font=FONT)
        resetBtn.pack(side='left', fill='both', padx=5)

        # confirm IDs button
        confirmBtn = tk.Button(
            buttonsRF, text="Confirm", command=self.confirm, font=FONT)
        confirmBtn.pack(side='right', fill='both', padx=5)

        # left buttons frame
        buttonsLF = tk.Frame(buttonsF)
        buttonsLF.pack(side='left', fill='both')

        # save IDs to file button
        saveBtn = tk.Button(buttonsLF, text="Save IDs",
                            command=self.save, font=FONT)
        saveBtn.pack(side='left', fill='both', padx=5)

        # load IDs from file button
        loadBtn = tk.Button(
            buttonsLF, text="Load Saved IDs", command=self.load, font=FONT)
        loadBtn.pack(side='right', fill='both', padx=5)

        # add an update listener to sensor data locked variable
        self.app.lockedSensorData.addUpdateStateListener(
            "sensorSetup", self.sensorDataChange)

    def idChange(self, id):
        """ called on change to any of the spinboxes """
        phynsb = self.sensorIDs[id-1]  # get physicalSpinbox variable
        try:
            v = phynsb.get()  # get value
        except Exception as e:
            return
        vals = [i for i in range(1, self.sensorCount+1)
                if i != v]  # get other values
        other: tk.IntVar = None  # init
        # print(id-1, v, vals)
        for sb in self.sensorIDs:  # iterate over sp variables
            val = sb.get()  # get value
            # print(v, val, vals)
            if val in vals:  # if in list
                vals.remove(val)  # remove from list
            if val == v and not sb == phynsb:  # if same value and not same variable
                other = sb  # save variable
        # print(vals)
        if len(vals) == 0:  # if no values left (changed max to max or min to min)
            return  # exit
        other.set(vals[0])  # set the other variable to the previous value

    def sensorDataChange(self, name, data):
        # print(data)
        for tempL, IDSB in zip(self.sensorTemps, self.sensorIDs):  # enumerate over data
            tempL.set(f"{data[IDSB.get() - 1]}C°")
            # index = self.sensorIDs[i].get()-1  # get ID at index
            # self.sensorTemps[index].set(f"{d}C°")  # set the temperature

    def reset(self):
        for i, id in enumerate(self.sensorIDs):  # enumerate over IDs
            id.set(i+1)  # set to consecutive index+1

    def confirm(self):
        m = [id.get() for id in self.sensorIDs]
        # print(m)
        self.app.lockedSensorData.removeUpdateStateListener("sensorSetup")
        self.app.message(APP_CODE_SENSOR_IDS_SET, m)

    def getVals(self):
        ids = ""  # save string
        try:
            # join ID values with \n
            ids = "\n".join([str(id.get()) for id in self.sensorIDs])
        except tk.TclError as e:  # if any error
            messagebox.showerror(
                "Error Reading IDs", "One or more of the IDs has an invalid value!\nPlease make sure all of the IDs are valid and try again.")
        return ids

    def getSaveData(self) -> tuple[Any, bool]:
        d = self.getVals()
        return (d, not d is None)

    def onSaveSuccess(self):
        messagebox.showinfo(
            "Success", "Saved IDs successfully.\nUse 'Load Saved IDs' button to load them back.")

    def onLoadFailure(self):
        messagebox.showerror(
            "Error Loading Saved IDs", f"Could not find save file!\nTry looking for a file called {SAVE_FILE}.")

    def parseData(self, data: str) -> bool:
        try:
            data = [int(i) for i in data.split("\n")]  # try parse
        except ValueError:  # any parse errors
            # inform user of corrupted save file
            messagebox.showerror(
                "Error Loading Saved IDs", "Error occured while parsing saved IDs.\nSave data is corrupt or incorrect.")
            return False

        # enumerate over zipped data and IDs
        for i, (val, id) in enumerate(zip(data, self.sensorIDs)):
            id.set(val)  # set values
        for j in range(i+1, len(self.sensorIDs)):  # iterate over remaining IDs(if any)
            self.sensorIDs[j].set(j+1)  # set to consecutive values

        return True
