from lib.imports import *
from lib.defines import *


class ScrollableFrame(ttk.Frame):
    """ from https://blog.teclado.com/tkinter-scrollable-frames/, a frame with a scrollable area.

    for adding widgets, use instance.scrollable_frame as master/parent """

    def __init__(self, container, height=None, width=None, vertical=True, horizontal=False, *args, **kwargs):
        super().__init__(container, *args, **kwargs)
        parent = tk.Frame(self)  # set parent frame for canvas and scrollbar
        parent.pack(side='top', fill='both', expand=True)
        canvas = tk.Canvas(parent, height=height, width=width)  # canvas

        # vertical and horizontal scrollbars if flags are set
        if vertical:
            yscrollbar = ttk.Scrollbar(
                parent, orient="vertical", command=canvas.yview)
        if horizontal:
            xscrollbar = ttk.Scrollbar(
                self, orient='horizontal', command=canvas.xview)

        # the actual scrollable frame the items are added onto
        self.scrollable_frame = ttk.Frame(canvas)

        # binds the scrollable frame reshape to the canvas bounding box
        self.scrollable_frame.bind(
            "<Configure>",
            lambda e: canvas.configure(
                scrollregion=canvas.bbox("all")
            )
        )

        # make the canvas
        canvas.create_window(
            (0, 0), window=self.scrollable_frame, anchor="nw")

        # configure scrollbars if flags are set
        if vertical:
            canvas.configure(yscrollcommand=yscrollbar.set)
        if horizontal:
            canvas.configure(xscrollcommand=xscrollbar.set)

        # pack canvas
        canvas.pack(side="left", fill="both", expand=True)

        # configure scrollbars if flags are set
        if vertical:
            yscrollbar.pack(side="right", fill="y")
        if horizontal:
            xscrollbar.pack(side='bottom', fill='x')


class StackedFrame(ttk.Frame):
    """ stacked windows layout """

    # dict[str, tuple[type, tk.Frame, dict[str, object], tk.Button]]

    def __init__(self, container, frames={}, pack_args={}):
        """
        @param frame: dict[str, tuple[type, tk.Frame, dict[str, object], tk.Button]]
        """
        super().__init__(container)
        self.pack_args = pack_args
        self.frames = {}  # dict of frames with names and such
        self.selected: str = ""  # the selected window

        self.buttonFrame = ScrollableFrame(
            self, height=25, horizontal=True, vertical=False)  # the scrollable frame of the buttons
        self.windowFrame = tk.Frame(self)  # the frame for the windows
        self.frames = {k: v for k, v in frames.items()}  # copy the frames list
        self.bg_label = tk.Label(self.windowFrame, text="")
        self.repack()  # repack

    def repack(self):
        """ repacks everything """

        # iterate on copy of self.frames
        for i, (name, (widgetClass, frame, options, button)) in enumerate(self.frames.copy().items()):
            # print(name, widgetClass, frame, options, button)
            if button == None:  # if button is not set
                # make button
                button = tk.Button(
                    self.buttonFrame.scrollable_frame, text=name, justify='center', command=lambda name=name: self.switchToWindow(name), height=1, font=FONT)
                button.pack(side='left')  # pack to the left
            # init the frame class with the given options
            if frame == None:
                frame = widgetClass(self.windowFrame, **options)
                frame.grid(row=0, column=0)  # set it to grid 0

            self.frames[name] = (widgetClass, frame,
                                 options, button)  # save data
        self.bg_label.place(x=0, y=14, relwidth=1, relheight=1, height=-14)
        self.buttonFrame.pack(fill='x', side='top', padx=5,
                              pady=0)  # pack the button frame
        self.windowFrame.pack(fill='both', side='bottom',
                              padx=5, pady=0)  # pack the window frame
        self.pack(**self.pack_args)  # pack the widget
        # switch to the first window
        if len(self.frames) > 0:
            self.switchToWindow(list(self.frames.keys())[0])

    def switchToWindow(self, windowName: str):
        """ switch to the given window """
        # print("switch to window", windowName)
        f = self.frames[windowName][1]  # get the frame
        if not f is None:  # if frame is set
            self.bg_label.tkraise()
            f.tkraise()  # raise it

            if self.selected != "":  # if selected set
                # set the selected window to normal
                self.frames[self.selected][-1].configure(state='normal')
            self.selected = windowName  # set selected to windowName
            # set the selected window to disabled
            self.frames[self.selected][-1].configure(state='disabled')
        else:  # if not set
            raise NameError(
                f"Given window does not have an initialized Frame. Window name: {f}")  # error

    def addWindow(self, name: str, widget: type, initOptions: dict[str, object]) -> tuple[type, tk.Frame, dict[str, Any], tk.Button]:
        """ adds a window to the widget """
        if name in self.frames:  # if name already exists
            raise NameError(
                f"Given window name already exists. Window name: {name}")  # error
        self.frames[name] = (widget, None, initOptions, None)  # add name
        self.repack()  # repack everything
        return self.frames[name]

    def removeWindow(self, name: str):
        """ remove a window of the given name from the widget """
        if not name in self.frames:  # if name does not exist
            raise NameError(
                f"Given window name does not exist. Window name: {name}")  # error
        del self.frames[name]  # delete the window
        self.repack()  # repack everything


class FastSpinbox(tk.Spinbox):
    """ same as tk.Spinbox, but the longer you hold the button for, thw faster it scrolls """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.interval = 0
        self.default_increment = kwargs["increment"] if "increment" in kwargs else 1
        self.fast_increment = self.default_increment*10
        self.holds = False
        # self["values"] = [i for i in range(32000)]
        # self.pack()
        self.bind('<ButtonPress-1>', self.on_press)
        self.bind('<ButtonRelease-1>', self.on_release)

    def checkInterval(self):
        if self.holds:  # if button is still held
            self.interval += 1  # advance interval
        else:  # if not held
            self.interval = 0  # reset
            return  # exit

        if self.interval < 3:  # if interval is less than 3
            # wait a second and check again
            self.after(1000, self.checkInterval)
        elif self.interval < 5:  # if interval is =>3 && <5 increase speed, and check again
            self.config(repeatdelay=1, repeatinterval=1,
                        increment=self.default_increment)
            self.after(1000, self.checkInterval)
        else:  # if interval is >= 5 increase speed even more
            self.config(increment=self.fast_increment)

    def on_press(self, event=None):
        # print(self.interval)
        self.interval = 0  # reset interval on click
        self.holds = True  # set button held
        self.after(1000, self.checkInterval)  # wait a second and check held

    def on_release(self, event=None):
        self.holds = False  # set not held
        self.interval = 0  # reset interval
        self.config(repeatdelay=100, repeatinterval=100,
                    increment=self.default_increment)  # reset settings


class ConnectionModes(Enum):
    """ modes of connection with the arduino board """
    wait = bytes([3])
    read = bytes([4])
    setup = bytes([5])


def connectToBus(port, busSpeed=9600):
    """ shortcut to connect to the Arduino serial port """
    ser = serial.Serial(port, busSpeed, timeout=TIMEOUT_DEFAULT)
    return ser


def serial_ports():
    """ Lists serial port names

        :raises EnvironmentError:
            On unsupported or unknown platforms
        :returns:
            A list of the serial ports available on the system
    """
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(2, 256) if i != 3]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    # print(ports)
    for port in ports:
        try:
            # print(port)
            s = serial.Serial(port)
            name = s.name
            s.close()
            result.append((port, name))
            # print("got port")
        except (OSError, serial.SerialException):
            pass
    return result


def isNumeric(value):
    # print(value)
    try:
        float(value)
        int(value)
    except:
        return False
    return True


class Export:
    """ basic OOP class for exporting data from class
    @ set Data to data to be exported or set data to a callable returning the data to be returned """

    Data = None
    """the data to export, can be a function for getting the data"""

    def exportData(self) -> str:
        """ returns the data from the class, DATA WILL BE IN FORM OF STRING """
        return self.Data if not isinstance(self.Data, Callable) else self.Data()


class Clearable:
    def Clear(self):
        return False
