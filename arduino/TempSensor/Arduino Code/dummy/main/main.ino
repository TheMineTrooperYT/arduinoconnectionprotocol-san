
#include <Arduino.h>
#include "HardwareSerial.h"

// #define DEBUG

/// SENSOR DIGITAL PIN #
#define OneWirePin 7

#define BAUD 9600

#define addrLen 8

#define sensorC -1

#define state_wait 3 // general states
#define state_read 4
#define state_setup 5

#define ss_init1 6 // setup state sub states
#define ss_init2 7 // setup state sub states

#define LLED 13

#define decimalAccuracy 10

#define setupSend currentState == state_setup &&setupState == ss_send

#define MAX_BYTE 0xFF
#define BYTE_BIT_COUNT 8
#define BYTES_PER_LONG 4

#ifdef DEBUG
#define send(d) Serial.print(d)
#define sendF(d) \
    send(d);     \
    sendEOF
#define sendEOF Serial.println()
#define newline sendEOF;
#define log(d) \
    send(d);   \
    newline
#else
#define send(d) Serial.write(d)
#define sendF(d) \
    send(d);     \
    Serial.flush()
#define sendEOF        \
    Serial.write(EOF); \
    Serial.flush()
#define newline
#define log(d)
#endif

#define SHORT 300
#define LONG SHORT * 4
int morseCode[10][5]{
    {LONG, LONG, LONG, LONG, LONG},      // 0
    {SHORT, LONG, LONG, LONG, LONG},     // 1
    {SHORT, SHORT, LONG, LONG, LONG},    // 2
    {SHORT, SHORT, SHORT, LONG, LONG},   // 3
    {SHORT, SHORT, SHORT, SHORT, LONG},  // 4
    {SHORT, SHORT, SHORT, SHORT, SHORT}, // 5
    {LONG, SHORT, SHORT, SHORT, SHORT},  // 6
    {LONG, LONG, SHORT, SHORT, SHORT},   // 7
    {LONG, LONG, LONG, SHORT, SHORT},    // 8
    {LONG, LONG, LONG, LONG, SHORT},     // 9
};

void blink(uint8_t pin, bool toggleOn = true, int d = 500);
void quickBlink(uint8_t pin, uint8_t count = 3, bool toggleOn = true, int d = 100);
int getSerialInput(bool clear = true);
void clearSerialBuffer();
bool inputIsState(int input);
bool inputIsSubState(int input);
void ProcessInput(int input);
void SendTemperatures();
void PopulateTemperatureBuffer();
void morse(unsigned int n, uint8_t pin)
{
    do
    {
        for (int i = 0; i < 5; i++)
        {
            digitalWrite(pin, HIGH);
            delay(morseCode[n % 10][i]);
            digitalWrite(pin, LOW);
            delay(SHORT / 2);
        }
        delay(LONG);
        n = n > 0 ? n / 10 : n;
    } while (n > 0);
    delay(LONG);
}

byte currentState = state_wait; // the current state
byte setupState = ss_init1;     // the setup state sub state

byte *temperatureBuffer;
int bufferSize;

#define sensor_count 6

long int start = 0;
long int stop = 0;
void setup()
{
    Serial.begin(BAUD); // begin the serial connection

    bufferSize = sensor_count * 2 + BYTES_PER_LONG;
    temperatureBuffer = new byte[bufferSize]; // init temperature buffer
    pinMode(LLED, OUTPUT);
    digitalWrite(LLED, LOW);

// quickBlink(LLED, 5);
#ifndef DEBUG
    currentState = state_wait;
#else
    currentState = state_setup;
#endif
    setupState = ss_init1;
    clearSerialBuffer();
    log("test");
    log(sensor_count);
}

void loop()
{
    if (currentState == state_setup && setupState == ss_init1)
    {
        sendF(sensor_count);
        delay(500);
    }

    if (Serial.available() > 0) // got any data (switch state)
    {
        ProcessInput(getSerialInput());
    }

    if (currentState == state_wait) // if in wait state
    {
        return; // return immediately
    }

    if (currentState == state_read) // if in setup send state or in read state
    {
        SendTemperatures();
        delay(1000);
    }
}

void blink(uint8_t pin, bool toggleOn = true, int d = 500)
{
    digitalWrite(pin, toggleOn ? HIGH : LOW);
    delay(d);
    digitalWrite(pin, toggleOn ? LOW : HIGH);
}

void quickBlink(uint8_t pin, uint8_t count = 3, bool toggleOn = true, int d = 100)
{
    for (int i = 0; i < count; i++)
    {
        blink(pin, toggleOn, d);
        delay(d);
    }
}

int getSerialInput(bool clear = true)
{
    if (Serial.available() == 0)
        return -1;
    int v = Serial.read();
    // if (v > state_setup)
    //     return -1;
    clearSerialBuffer();
    return v;
}

void clearSerialBuffer()
{
    while (Serial.available() > 0)
        Serial.read();
}

bool inputIsState(int input)
{
    return input == state_wait || input == state_read || input == state_setup;
}
bool inputIsSubState(int input)
{
    return input == ss_init1 || input == ss_init2;
}

void ProcessInput(int input)
{
    if (!inputIsState(input))
    {
#ifndef DEBUG
        morse(input, LLED);
#else
        Serial.print("error input: ");
        Serial.println(input);
#endif
        return;
    }

    if (currentState == state_read) // if in read
    {
        if (input == state_wait)
        {
            currentState = state_wait;
            return;
        }
    }
    else if (currentState == state_wait) // if in wait
    {

        currentState = input;

        if (currentState == state_setup)
        {
            setupState = ss_init1;
            send(sensor_count);
            newline
            // quickBlink(LLED, 5, true, 500);
        }

        if (currentState != state_read)
            return;
    }
    else // if in sensor setup
    {
        if (setupState == ss_init1) // if in init sub state -- meaning gui received the sensor count
        {
            setupState = ss_init2; // switch to init2 state, wait for buffer to clear
            // quickBlink(LLED, 2);
        }
        if (setupState == ss_init2) // if in init2 sub state, meaning buffer has been cleared
        {
            sendEOF;
            delay(1000);
            // quickBlink(LLED, 4);
            currentState = state_read; // switch to read state
        }
    }
}

/// @brief converts int into array of bytes
/// @param val value to convert
/// @param bytes array of bytes to store results in
void intToBytes(long val, byte bytes[BYTES_PER_LONG])
{
    for (int i = 0; i < BYTES_PER_LONG; i++)
    {
        log(val);
        log(i);
        // log(sizeof(byte));
        bytes[i] = val & MAX_BYTE;
        if (val > 0)
            val >>= BYTE_BIT_COUNT;
    }
}

void SendTemperatures()
{
    PopulateTemperatureBuffer();

    // digitalWrite(pin_output, HIGH);

    for (int i = 0; i < bufferSize; i++)
    {
        send(temperatureBuffer[i]);
        log(' ');
    }
    sendEOF;
    // digitalWrite(pin_output, LOW);
}

byte convertedtime[BYTES_PER_LONG];
void PopulateTemperatureBuffer()
{

    float temps[] = {20, 21, 22, 23, 24, 25};
    stop = millis();
    long int dt = stop - start;
    start = stop;
    log("Time: ");
    log(dt);
    newline;
    intToBytes(dt, convertedtime);

    bool zero = false;
    // byte *buffer = new byte[si->sensor_count * 2];            // init buffer to 2 * sensor count
    int j = 0;
    for (int i = 0; i < sensor_count; i++, j += 2) // iterate over sensor count, and iterate j as index in buffer
    {
        float temp = temps[i];                                  // get temperature
        byte numb = (byte)temp;                                 // get rounded number
        byte decimal = (byte)((temp - numb) * decimalAccuracy); // get decimal * accuracy
        temperatureBuffer[j] = numb;                            // set rounded number to j index
        temperatureBuffer[j + 1] = decimal;                     // set decimal to j index + 1
    }
    for (int i = 0; i < BYTES_PER_LONG; i++, ++j)
    {
        temperatureBuffer[j] = convertedtime[i];
    }

    // delete[] temps;
}