#include "modbus_master.h"

#include "button_click.h"

/*

    Copyrights ----
        Project owned by TheMineTrooper on Gitlab (https://gitlab.com/TheMineTrooperYT/arduinoconnectionprotocol-san)
        No forks or anything like that is allowed.
        No copies of the project is allowed.
        No redistribution of the project anywhere.

        If you want to use this code in your project, please contact me beforehand.
    
*/

//////////////////// Port information ///////////////////
#define baud 9600
#define timeout 1000
#define polling 200 // the scan rate
#define retry_count 10

#define clamp(v, b, a) max(b, min(v, a))

// used to toggle the receive/transmit pin on the driver
#define TxEnablePin 2

// #define BLINKLED

// The total amount of available memory on the master to store data
#ifdef BLINKLED
#define __no 1
#else
#define __no 6
#endif
#define TOTAL_NO_OF_REGISTERS __no

#define BTN 8

// This is the easiest way to create new packets
// Add as many as you want. TOTAL_NO_OF_PACKETS
// is automatically updated.
enum
{
    // READY_SLAVE,
    READ_REGS,
    READ_REGS1,
    READ_REGS2,
    READ_REGS3,
    // SEND_TO_SLEEP,
    TOTAL_NO_OF_PACKETS // leave this last entry
};

#define SLAVE_ID 1
#define MAXID 32

byte ID = SLAVE_ID;

void set_packets();

btn_click increase = btn_click(
    4, [] { ID = clamp(ID + 1, SLAVE_ID, MAXID);print2("Id: ",ID);set_packets(); }, OnClick);
btn_click reset = btn_click(
    7, [] { ID = SLAVE_ID;print2("Id (reset): ",ID);set_packets(); }, OnClick);

// Create an array of Packets to be configured
Packet packets[TOTAL_NO_OF_PACKETS];

constexpr unsigned int actual_no_of_regs = TOTAL_NO_OF_REGISTERS * 4 + 1;
// Masters register array
unsigned int regs[actual_no_of_regs];

btn_click print = btn_click(
    8,
    [] {
        printlist(regs, actual_no_of_regs, "regs"); // print regs array
        print1();                                   // add space
    },
    NULL,
    NULL,
    [] { modbus_update(); }, [] { return GetState() == IDLE; },
    OnClick | WhileReleased);

void set_packets()
{
    // // Initialize each packet
    // // clang-format off
    // //              |  the packet index  |the slave|   the function code   |   the address to read from   | hm registers to read on  |  the local address to write
    // //                                        id                                    on the slave                   the slave                    the data to
    // modbus_construct(&packets[READ_REGS] ,    ID,    READ_HOLDING_REGISTERS, 0                            , 1 + TOTAL_NO_OF_REGISTERS, 0                            ); // configure first unit registery read packet
    // modbus_construct(&packets[READ_REGS1],    ID,    READ_HOLDING_REGISTERS, 1 + TOTAL_NO_OF_REGISTERS    ,     TOTAL_NO_OF_REGISTERS, 1 + TOTAL_NO_OF_REGISTERS    ); // configure second unit registery read packet
    // modbus_construct(&packets[READ_REGS2],    ID,    READ_HOLDING_REGISTERS, 1 + TOTAL_NO_OF_REGISTERS * 2,     TOTAL_NO_OF_REGISTERS, 1 + TOTAL_NO_OF_REGISTERS * 2); // configure third unit registery read packet
    // modbus_construct(&packets[READ_REGS3],    ID,    READ_HOLDING_REGISTERS, 1 + TOTAL_NO_OF_REGISTERS * 3,     TOTAL_NO_OF_REGISTERS, 1 + TOTAL_NO_OF_REGISTERS * 3); // configure fourth unit registery read packet
    // // clang-format on

    // clang-format off
    packets[READ_REGS ].id = ID;
    packets[READ_REGS1].id = ID;
    packets[READ_REGS2].id = ID;
    packets[READ_REGS3].id = ID;
    // clang-format on
}

void setup()
{
    for (int i = 0; i < actual_no_of_regs; i++)
    {
        regs[i] = 0;
    }

    //print.on_release_event ;
    // print.on_click_event = [] {
    //     for (int i = 0; i < actual_no_of_regs; i++) // run on register number
    //     {
    //         Serial.print("reg "); // print
    //         Serial.print(i);
    //         Serial.print(": ");
    //         Serial.println(regs[i]);
    //     }
    //     Serial.println("\n"); };

    // Initialize each packet
    // clang-format off
    //              |  the packet index  |the slave|   the function code   |   the address to read from   | hm registers to read on  |  the local address to write
    //                                        id                                    on the slave                   the slave                    the data to
    modbus_construct(&packets[READ_REGS] , SLAVE_ID, READ_HOLDING_REGISTERS, 0                            , 1 + TOTAL_NO_OF_REGISTERS, 0                            ); // configure first unit registery read packet
    modbus_construct(&packets[READ_REGS1], SLAVE_ID, READ_HOLDING_REGISTERS, 1 + TOTAL_NO_OF_REGISTERS    ,     TOTAL_NO_OF_REGISTERS, 1 + TOTAL_NO_OF_REGISTERS    ); // configure second unit registery read packet
    modbus_construct(&packets[READ_REGS2], SLAVE_ID, READ_HOLDING_REGISTERS, 1 + TOTAL_NO_OF_REGISTERS * 2,     TOTAL_NO_OF_REGISTERS, 1 + TOTAL_NO_OF_REGISTERS * 2); // configure third unit registery read packet
    modbus_construct(&packets[READ_REGS3], SLAVE_ID, READ_HOLDING_REGISTERS, 1 + TOTAL_NO_OF_REGISTERS * 3,     TOTAL_NO_OF_REGISTERS, 1 + TOTAL_NO_OF_REGISTERS * 3); // configure fourth unit registery read packet
    // clang-format on

    // Initialize the Modbus Finite State Machine
    modbus_configure(&Serial, baud, SERIAL_8N1, timeout, polling, retry_count, TxEnablePin, packets, TOTAL_NO_OF_PACKETS, regs);

    pinMode(LED_BUILTIN, OUTPUT);
}

void loop()
{
    btn_click::Loop();
}

#undef __no