/*

    Copyrights ----
        Project owned by TheMineTrooper on Gitlab (https://gitlab.com/TheMineTrooperYT/arduinoconnectionprotocol-san)
        No forks or anything like that is allowed.
        No copies of the project is allowed.
        No redistribution of the project anywhere.

        If you want to use this code in your project, please contact me beforehand.
    
*/

#include <Arduino.h>

#pragma region prints
#define print9(a1, a2, a3, a4, a5, a6, a7, a8, a9) \
    Serial.print(a1);                              \
    Serial.print(a2);                              \
    Serial.print(a3);                              \
    Serial.print(a4);                              \
    Serial.print(a5);                              \
    Serial.print(a6);                              \
    Serial.print(a7);                              \
    Serial.print(a8);                              \
    Serial.println(a9);                            \
    ;
#define print8(a1, a2, a3, a4, a5, a6, a7, a8) \
    Serial.print(a1);                          \
    Serial.print(a2);                          \
    Serial.print(a3);                          \
    Serial.print(a4);                          \
    Serial.print(a5);                          \
    Serial.print(a6);                          \
    Serial.print(a7);                          \
    Serial.println(a8);                        \
    ;
#define print7(a1, a2, a3, a4, a5, a6, a7) \
    Serial.print(a1);                      \
    Serial.print(a2);                      \
    Serial.print(a3);                      \
    Serial.print(a4);                      \
    Serial.print(a5);                      \
    Serial.print(a6);                      \
    Serial.println(a7);                    \
    ;
#define print6(a1, a2, a3, a4, a5, a6) \
    Serial.print(a1);                  \
    Serial.print(a2);                  \
    Serial.print(a3);                  \
    Serial.print(a4);                  \
    Serial.print(a5);                  \
    Serial.println(a6);                \
    ;
#define print5(a1, a2, a3, a4, a5) \
    Serial.print(a1);              \
    Serial.print(a2);              \
    Serial.print(a3);              \
    Serial.print(a4);              \
    Serial.println(a5);            \
    ;
#define print4(a1, a2, a3, a4) \
    Serial.print(a1);          \
    Serial.print(a2);          \
    Serial.print(a3);          \
    Serial.println(a4);        \
    ;
#define print3(a1, a2, a3) \
    Serial.print(a1);      \
    Serial.print(a2);      \
    Serial.println(a3);    \
    ;
#define print2(a1, a2)  \
    Serial.print(a1);   \
    Serial.println(a2); \
    ;
#define print1(a1)      \
    Serial.println(a1); \
    ;

#define printlist(lst, size, name)               \
    for (int __i__ = 0; __i__ < size; __i__++)   \
    {                                            \
        print5(name, '[', __i__, "]: ", lst[__i__]); \
    };
#pragma endregion

#pragma region decleration

#define __btn_pressed true
#define __btn_released false

enum ButtonModes
{
    None = 0,
    // clang-format off
    OnClick              = 1,
    OnRelease            = 1 << 1,
    OnHold               = 1 << 2,
    WhileReleased = 1 << 3,
    // clang-format on
};

class btn_click;

class base
{
protected:
    inline static btn_click **buttons;
    inline static unsigned int __count;

    unsigned long click_buffer_clock;

    static const unsigned int click_buffer = 50; // milliseconds

public:
    base(btn_click *button, byte pin)
    {
        pinMode(pin, INPUT);    // set pin mode
        digitalWrite(pin, LOW); // try to activate pullup resistors if they exist in board
        if (buttons == NULL)    // if not initialized (first)
        {
            __count = 1;                       // set count
            buttons = (btn_click **)malloc(1); // create array of size 1
            buttons[0] = button;               // set first item to given button
        }
        else // initialized
        {
            __count++;                                         // advance count
            buttons = (btn_click **)realloc(buttons, __count); // re-create array with new size
            buttons[__count - 1] = button;                     // set button to last position
        }

        click_buffer_clock = 0; // init clock counter
    }

    ~base()
    {
        free(buttons); // free the allocated memory
    }
};

class btn_click : public base
{
private:
    bool btn_state; // the state of the button, true=pressed, false=released

    byte btn_pin; // the button pin

    ButtonModes btn_mode; // the button mode

    bool check_state(ButtonModes); // checks the state of the mode, can i call the event?

    void _loop(bool = true); // the main loop function. call in the main update/loop function of the code

    // the default function for the different events. is used if null is recieved.
    inline static void (*default_on_click_event)() = [] { Serial.println("OnClick"); };
    inline static void (*default_on_release_event)() = [] { Serial.println("OnRelease"); };
    inline static void (*default_on_hold_event)() = [] { Serial.println("OnHold"); };
    inline static void (*default_while_released_event)() = [] { Serial.println("WhileReleased"); };
    inline static bool (*default_extra_conditions)() = [] { return true; };

public:
#pragma region Ctors
    /*
    
    @param byte pin: the pin the button looks on
    @param void (*on_click_event)() = NULL:  the on click event function
    @param void (*on_release_event)() = NULL:  the on release event function
    @param void (*on_hold_event)() = NULL:  the on hold event function
    @param void (*while_released_event)() = NULL:  the while released event function
    @param bool (*extra_conditions)() = NULL:  the extra conditions function
    @param int = ButtonModes::OnHold: the button's mode
    */
    btn_click(byte pin, int = ButtonModes::OnHold);
    btn_click(byte pin, void (*on_click_event)(), int = ButtonModes::OnHold);
    btn_click(byte pin, void (*on_click_event)(), void (*on_release_event)(), int = ButtonModes::OnHold);
    btn_click(byte pin, void (*on_click_event)(), void (*on_release_event)(), void (*on_hold_event)(), int = ButtonModes::OnHold);
    btn_click(byte pin, void (*on_click_event)(), void (*on_release_event)(), void (*on_hold_event)(), void (*while_released_event)(), int = ButtonModes::OnHold);
    btn_click(byte pin, void (*on_click_event)(), void (*on_release_event)(), void (*on_hold_event)(), void (*while_released_event)(), bool (*extra_conditions)(), int = ButtonModes::OnHold);
#pragma endregion

    // returns true if the button is pressed
    bool IsPressed() { return btn_state == __btn_pressed; }
    //
    // the on click event
    void (*on_click_event)() = [] { Serial.println("OnClick"); };

    // the on release event
    void (*on_release_event)() = [] { Serial.println("OnRelease"); };

    // the on hold event
    void (*on_hold_event)() = [] { Serial.println("OnHold"); };

    // the while released event
    void (*while_released_event)() = [] { Serial.println("WhileReleased"); };

    // the extra conditions function
    bool (*extra_conditions)() = [] { return true; };

    // runs on the button array and loops each one
    static void Loop(bool event = true)
    {
        for (int i = 0; i < __count; i++) // run on array count
        {
            buttons[i]->_loop(event); // call loop
        }
    }
};

#pragma endregion

#pragma region definition
#pragma region Ctors

btn_click::btn_click(byte pin, int btn_mode)
    : btn_pin(pin),
      btn_mode((ButtonModes)btn_mode),
      btn_state(false),
      base(this, pin)
{
}

btn_click::btn_click(byte pin, void (*on_click_event)(), void (*on_release_event)(), void (*on_hold_event)(), void (*while_released_event)(), bool (*extra_conditions)(), int btn_mode)
    : btn_pin(pin),
      btn_mode((ButtonModes)btn_mode),
      btn_state(false),
      extra_conditions(extra_conditions != NULL ? extra_conditions : default_extra_conditions),
      on_click_event(on_click_event != NULL ? on_click_event : default_on_click_event),
      on_hold_event(on_hold_event != NULL ? on_hold_event : default_on_hold_event),
      on_release_event(on_release_event != NULL ? on_release_event : default_on_release_event),
      while_released_event(while_released_event != NULL ? while_released_event : default_while_released_event),
      base(this, pin) {}

btn_click::btn_click(byte pin, void (*on_click_event)(), void (*on_release_event)(), void (*on_hold_event)(), void (*while_released_event)(), int btn_mode)
    : btn_pin(pin),
      btn_mode((ButtonModes)btn_mode),
      btn_state(false),
      on_click_event(on_click_event != NULL ? on_click_event : default_on_click_event),
      on_hold_event(on_hold_event != NULL ? on_hold_event : default_on_hold_event),
      on_release_event(on_release_event != NULL ? on_release_event : default_on_release_event),
      while_released_event(while_released_event != NULL ? while_released_event : default_while_released_event),
      base(this, pin) {}

btn_click::btn_click(byte pin, void (*on_click_event)(), void (*on_release_event)(), void (*on_hold_event)(), int btn_mode)
    : btn_pin(pin),
      btn_mode((ButtonModes)btn_mode),
      btn_state(false),
      on_click_event(on_click_event != NULL ? on_click_event : default_on_click_event),
      on_hold_event(on_hold_event != NULL ? on_hold_event : default_on_hold_event),
      on_release_event(on_release_event != NULL ? on_release_event : default_on_release_event),
      base(this, pin) {}

btn_click::btn_click(byte pin, void (*on_click_event)(), void (*on_release_event)(), int btn_mode)
    : btn_pin(pin),
      btn_mode((ButtonModes)btn_mode),
      btn_state(false),
      on_click_event(on_click_event != NULL ? on_click_event : default_on_click_event),
      on_release_event(on_release_event != NULL ? on_release_event : default_on_release_event),
      base(this, pin) {}

btn_click::btn_click(byte pin, void (*on_click_event)(), int btn_mode)
    : btn_pin(pin),
      btn_mode((ButtonModes)btn_mode),
      btn_state(false),
      on_click_event(on_click_event != NULL ? on_click_event : default_on_click_event),
      base(this, pin) {}

#pragma endregion

void btn_click::_loop(bool trigger_events)
{
    bool btn_read = digitalRead(btn_pin); // read from pin

    if (btn_read && (extra_conditions == NULL ? true : extra_conditions())) // if button is pressed and extra conditions returns true (or non-existent)
    {
        if (btn_state == __btn_pressed) // if pressed
        {
            // OnHold
            if (trigger_events && check_state(OnHold)) // is being kept pressed
                on_hold_event();                       // hold event
        }
        else // if released
        {
            if (millis() - click_buffer_clock > click_buffer) // if not in click buffer
            {
                btn_state = !btn_state; // flip button state
                // OnClick
                if (trigger_events && check_state(OnClick)) // if can trigger on click event
                {
                    on_click_event(); // trigger
                }
                click_buffer_clock = millis(); // reset buffer clock
            }
        }
    }
    else // if button is not pressed or extra conds returned false
    {
        if (btn_state == __btn_pressed) // if button is pressed
        {
            if (millis() - click_buffer_clock > click_buffer) // if not in click buffer
            {
                btn_state = !btn_state; // flip button state
                // OnRelease
                if (trigger_events && check_state(OnRelease)) // if can call event
                {
                    on_release_event(); // call event
                }
                click_buffer_clock = millis(); // reset buffer clock
            }
        }
        else // if released
        {
            // WhileRelesed
            if (trigger_events && check_state(WhileReleased)) // is being kept released, if can call while release event
                while_released_event();                       // call event
        }
    }
}

bool btn_click::check_state(ButtonModes mode)
{
    bool is_in_mode = (btn_mode & mode) != 0; // if flag is in the button's mode
    // print6("is in mode: ", is_in_mode, ", mode: ", mode, ", and result: ", (btn_mode & mode) != 0);
    if (!is_in_mode)  // if flag is missing, cant trigger event
        return false; // exit
    switch (mode)     // for each mode, reaturn true if event is not empty
    {
    case ButtonModes::OnClick:
        return on_click_event != NULL;
    case ButtonModes::OnRelease:
        return on_release_event != NULL;
    case ButtonModes::OnHold:
        return on_hold_event != NULL;
    case ButtonModes::WhileReleased:
        return while_released_event != NULL;
    default:
        break;
    }

    return false; // recieved invalid mode, cant call event
}

#pragma endregion

#undef __btn_pressed
#undef __btn_released